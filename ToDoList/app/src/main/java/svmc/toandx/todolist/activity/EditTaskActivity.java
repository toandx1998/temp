package svmc.toandx.todolist.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import svmc.toandx.todolist.R;
public class EditTaskActivity extends AppCompatActivity {

    private EditText editTextNote,editTextTitle;
    private TextView dueDateTv,remindDateTv,priorityTv,titleTv;
    private Button dueDateBtn,remindDateBtn,priorityBtn,submitBtn;
    private CheckBox statusCheckBox;
    private int priority;
    String title,note;
    long dueTime,reminderTime;
    private boolean completed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task);
        editTextTitle=(EditText) findViewById(R.id.edit_text_title);
        editTextNote=(EditText) findViewById(R.id.edit_text_note);
        dueDateTv=(TextView) findViewById(R.id.tv_due_date);
        remindDateTv=(TextView) findViewById(R.id.tv_reminder);
        dueDateBtn=(Button) findViewById(R.id.btn_due_date);
        remindDateBtn=(Button) findViewById(R.id.btn_reminder);
        priorityBtn=(Button) findViewById(R.id.btn_priority);
        priorityTv=(TextView) findViewById(R.id.tv_priority);
        statusCheckBox = (CheckBox) findViewById(R.id.checkbox_status);
        submitBtn=(Button) findViewById(R.id.btn_submit);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title = editTextTitle.getText().toString();
                note = editTextNote.getText().toString();
                completed = statusCheckBox.isChecked();
                Intent intent=new Intent();
                intent.putExtra("title",title);
                intent.putExtra("note",note);
                setResult(RESULT_OK,intent);
                finish();
            }
        });
        dueDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        remindDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

}
